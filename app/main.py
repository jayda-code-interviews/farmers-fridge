import pandas as pd
from scipy.spatial import distance_matrix
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2


class Places():
    def __init__(self, csv_file):
        self._csv_file = pd.read_csv(csv_file)
        self._number_of_places = len(self._csv_file)
        self._depot = 0
        self._number_of_drivers = 2

        self._df = self._csv_file.filter(['latitude (N)', 'longitude (N)'], axis=1)
        self._distance_matrix = distance_matrix(self._df.values, self._df.values)

        self._distance_scale = 1000

    @property
    def list(self):
        """Returns list of places in csv file as an object"""
        return self._csv_file

    @property
    def number_of_places(self):
        """Returns number of places"""
        return self._number_of_places

    @property
    def depot(self):
        """Returns index of depot"""
        return self._depot

    @property
    def number_of_drivers(self):
        """Returns index of depot"""
        return self._number_of_drivers

    @property
    def matrix(self):
        """Retuns scipy distance matrix"""
        return self._distance_matrix

    @property
    def distance_scale(self):
        """Returns distance scale factor"""
        return self._distance_scale

    def distance_callback(self, from_node, to_node):
        """Returns scaled interger distance between the two nodes"""
        return int(self._distance_matrix[from_node][to_node] * self._distance_scale)


class Routes():
    def __init__(self, places, assignment, routing):
        self._number_of_drivers = places.number_of_drivers
        self._names_of_places = places.list['name']
        self._assignment = assignment
        self._routing = routing
        self._scale = places.distance_scale

    def get_array(self):
        """Get the routes for an assignent and return as a list of lists."""
        assignment = self._assignment
        number_of_drivers = self._number_of_drivers
        routing = self._routing

        routes = []
        for driver in range(number_of_drivers):
            node = routing.Start(driver)
            route = []

            while not routing.IsEnd(node):
                index = routing.NodeToIndex(node)
                route.append(index)
                node = assignment.Value(routing.NextVar(node))
            routes.append(route)

        return routes

    def get_string(self):
        """Get the routes in a formatted string"""
        assignment = self._assignment
        names = self._names_of_places
        number_of_drivers = self._number_of_drivers
        routing = self._routing
        scale = self._scale

        output = "Total distance: {} miles\n\n".format(str(assignment.ObjectiveValue() / scale))

        for driver in range(number_of_drivers):
            route_number = driver
            index = routing.Start(route_number)

            route = ''
            while not routing.IsEnd(index):
                route += str(names[routing.IndexToNode(index)]) + ' -> '
                index = assignment.Value(routing.NextVar(index))
            route += str(names[routing.IndexToNode(index)])
            output += "Route: {}\n\n{}\n\n".format(driver, route)

        return output


def add_distance_dimension(routing, distance_evaluator):
    """Add distance contraints"""
    distance = "Distance"
    maximum_distance = 3000

    routing.AddDimension(distance_evaluator, 0, maximum_distance, True, distance)
    distance_dimension = routing.GetDimensionOrDie(distance)
    distance_dimension.SetGlobalSpanCostCoefficient(100)


def main():
    kiosks = Places('kiosk-cords.csv')

    routing = pywrapcp.RoutingModel(kiosks.number_of_places, kiosks.number_of_drivers, kiosks.depot)

    distance = kiosks.distance_callback
    routing.SetArcCostEvaluatorOfAllVehicles(distance)
    add_distance_dimension(routing, distance)

    search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    assignment = routing.SolveWithParameters(search_parameters)

    routes = Routes(kiosks, assignment, routing)

    print(routes.get_array())
    print(routes.get_string())


if __name__ == '__main__':
    main()
